import pandas as pd
from collections import Counter
import operator
from sklearn.cluster import KMeans
import numpy as np

#http://aimotion.blogspot.cz/2013/01/machine-learning-and-data-mining.html

def frequentItems(transactions, support):
    counter = Counter()
    for trans in transactions:
        counter.update(frozenset([t]) for t in trans)
    #print "frequentItems - counter", counter
    return set(item for item in counter if float(counter[item]) / len(transactions) >= support), counter


def generateCandidates(L, k):
    candidates = set()
    for a in L:
        for b in L:
            union = a | b
            if len(union) == k and a != b:
                candidates.add(union)
    return candidates


def filterCandidates(transactions, itemsets, support):
    counter = Counter()
    for trans in transactions:
        subsets = [itemset for itemset in itemsets if itemset.issubset(trans)]
        counter.update(subsets)
    return set(item for item in counter if float(counter[item]) / len(transactions) >= support), counter


def apriori(transactions, support):
    result = list()
    resultc = Counter()
    candidates, counter = frequentItems(transactions, support)
    #print "apriori - candidates:", candidates

    result += candidates
    resultc += counter
    k = 2
    while candidates:
        #print "apriori, while - candidates:", candidates
        candidates = generateCandidates(candidates, k)
        candidates, counter = filterCandidates(transactions, candidates, support)
        result += candidates
        resultc += counter
        k += 1
    resultc = {item: (float(resultc[item]) / len(transactions)) for item in resultc}
    return result, resultc


def list_powerset(lst):
    result = [[]]
    for x in lst:
        result.extend([subset + [x] for subset in result])
    return result

class Info(object):
    def __init__(self, visitID, mainConv, microConv, timeSum, scoreSum):
        self.visitID = visitID
        self.mainConv = mainConv
        self.microConv = microConv
        self.timeSum = timeSum
        self.scoreSum = scoreSum

    def __str__(self):
        return "visitID: " + str(self.visitID) + ", mainConv: " + str(self.mainConv) + ", microConv: " + str(self.microConv) + ", timeSum: " + str(self.timeSum) + ", scoreSum: " + str(self.scoreSum)

class Rule():
    def __init__(self, left, right, conf, conv, supp):
        self.left = left
        self.right = right
        self.conf = round(conf, 3)
        self.conv = round(conv, 3)
        self.supp = round(supp, 3)
        self.len = len(left)+len(right)

    def __str__(self):
        flag = False
        res = "Rule: ("
        for l in self.left:
            if flag:
                res = res + ", " + l
            else:
                res = res + l
                flag = True
        res = res + ") ---> ("
        flag = False
        for r in self.right:
            if flag:
                res = res + ", " + r
            else:
                res = res + r
                flag = True
        res = res + "), confidence: " + str(self.conf) + ", conviction: "
        if self.conv == INF:
            res = res + "Infinite"
        else:
            res = res + str(self.conv)
        res = res + ", support: " + str(self.supp)
        return res

def generateRules(frequentItemsets, supports, minConfidence):
    rules = []
    #print "generateRules - frequentItemsets:", frequentItemsets
    #print "generateRules - supports:", supports
    for s in frequentItemsets:
        for subset in frozenset(map(frozenset,list_powerset(list(s)))):
            if not subset:
                #print "Empty subset"
                continue
            #print "\n", supports[s]
            #print "s:", s
            #print "subset:", subset
            #print "Support subset:", supports[subset]

            tmpSet = set(s)
            tmpSub = set(subset)

            #print "tmpSub:", tmpSub
            for it in tmpSub:
                tmpSet.remove(it)
            if not tmpSet:
                #print "Empty tmpSet"
                continue
            tmpSetF = frozenset(tmpSet)
            #print "tmpSet:", tmpSet
            #print "Support set:", supports[s]
            #print "Support set-subset:", supports[tmpSetF]
            confidence = supports[s]/supports[tmpSetF]
            if confidence == 1:
                conviction = INF
            else:
                conviction = (1-supports[subset])/(1-confidence)
            if confidence >= minConfidence:
                #print "Rule:", tmpSet, "--->", tmpSub, "confidence =", confidence, "conviction =", conviction,"support", supports[s]
                rule = Rule(tmpSet, tmpSub, confidence, conviction, supports[s])
                rules.append(rule)
        #print "\n"
    return rules



mainConvList = ("APPLICATION", "CATALOG")
microConvList = ("DISCOUNT", "HOWTOJOIN", "INSURANCE", "WHOWEARE")
TRESHOLD_TIME = 60
TRESHOLD_PAGES = 2
NUM_CLUSTERS = 8
MIN_RATIO = 0.1
INF = 999

clicks = pd.read_csv("./clicks.csv")
sem = pd.read_csv("./search_engine_map.csv")
visitors = pd.read_csv("./visitors.csv")

result = visitors.merge(sem, on="Referrer")
result = result.sort_values(["VisitID"])
result.to_csv("result.csv", index=False)

visitsDict = {}
visitsList = []

mainConv = False
microConv = False
timeSum = clicks["TimeOnPage"].iloc[0]
scoreSum = clicks["PageScore"].iloc[0]
curID = 0;
prevID = clicks["VisitID"].iloc[0]
pgName = clicks["PageName"].iloc[0]
if pgName in mainConvList:
    mainConv = True
if pgName in microConvList:
    microConv = True

for i in range(1, clicks.__len__()):
    curID = clicks["VisitID"].iloc[i]
    if curID == prevID:
        timeSum = timeSum + clicks["TimeOnPage"].iloc[i]
        scoreSum = scoreSum + clicks["PageScore"].iloc[i]
        pgName = clicks["PageName"].iloc[i]
        if pgName in mainConvList:
            mainConv = True
        if pgName in microConvList:
            microConv = True
    else:
        #udelat polozku v dict
        tmpInfo = Info(prevID, mainConv, microConv, timeSum, scoreSum)
        row = [prevID, mainConv, microConv, timeSum, scoreSum]
        visitsDict[prevID] = tmpInfo
        visitsList.append(row)
        prevID = curID
        mainConv = False
        microConv = False
        timeSum = clicks["TimeOnPage"].iloc[i]
        scoreSum =  clicks["PageScore"].iloc[i]
        pgName = clicks["PageName"].iloc[i]
        if pgName in mainConvList:
            mainConv = True
        if pgName in microConvList:
            microConv = True

#ulozit posledni visit
tmpInfo = Info(prevID, mainConv, microConv, timeSum, scoreSum)
row = [prevID, mainConv, microConv, timeSum, scoreSum]
visitsDict[prevID] = tmpInfo
visitsList.append(row)

visits = pd.DataFrame(visitsList, columns=["VisitID", "MainConversion", "MicroConversion", "TotalTime", "TotalScore"])

'''for i in range(0, result.__len__()):
    visitID = result["VisitID"].iloc[i]
    print visitID, visitsList[i]
    continue

print visits
'''

df = result.merge(visits, on="VisitID")
df.to_csv("result.csv", index=False)

#print result

#odstranit navstevy kratsi nez TRESHOLD
df = df[df["TotalTime"] > TRESHOLD_TIME]
df = df[df["Length_pagecount"] > TRESHOLD_PAGES]
del df["VisitID"]           #id
del df["Referrer"]
del df["Length_seconds"]    #spatne hodnoty, spravne jsou v TotalTime
df["Hour"] = pd.cut(df["Hour"],6)
df["TotalTime"] = pd.cut(df["TotalTime"],5)
df["TotalScore"] = pd.cut(df["TotalScore"],5)
df["Length_pagecount"] = pd.cut(df["Length_pagecount"],3)

#print df

dataset = []
for index, row in df.iterrows():
    row = [ col + "=" +str(row[col]) for col in list(df)]
    dataset.append(row)

frequentItemsets, supports = apriori(dataset, 0.3)
#print "main - frequentItemsets:", frequentItemsets
#print "main - supports:", supports, "\n\n\n"
rules = generateRules(frequentItemsets, supports, 0.9)



print "Rules sorted by confidence (edited):"
sorted_rules = sorted(rules, key=operator.attrgetter('conf'))
i = 1
for rule in sorted_rules:
    if ("Type=nan" not in rule.left) and ("MicroConversion=False" not in rule.left) and ("MainConversion=False" not in rule.left) and ("MicroConversion=False" not in rule.right) and ("MainConversion=False" not in rule.right):
        print i, rule
        i = i + 1
print "\n\n"

'''
print "Rules sorted by conviction:"
sorted_rules = sorted(rules, key=operator.attrgetter('conv'))
i = 1
for rule in sorted_rules:
    print i, rule
    i = i + 1
print "\n\n"

print "Rules sorted by support:"
sorted_rules = sorted(rules, key=operator.attrgetter('supp'))
i = 1
for rule in sorted_rules:
    print i, rule
    i = i + 1
print "\n\n"

print "Rules sorted by rule length:"
sorted_rules = sorted(rules, key=operator.attrgetter('len'))
i = 1
for rule in sorted_rules:
    print i, rule
    i = i + 1
print "\n\n"
'''

# zacatek analyzy CLUSTERING--------------------------------------------------------------------------------------------

topics = []
allTopics = set()

clicksGrouped = clicks.ix[:, ["VisitID", "TopicName"]].groupby("VisitID")
for key, item in clicksGrouped:
    s = set()
    for i in item["TopicName"]:
        s.add(i)
        allTopics.add(i)
    topics.append(list(s))
allTopics = list(allTopics)


numOfTopics = allTopics.__len__()
#print numOfTopics
#print allTopics

numList = []
for t in topics:
    tmpList = []
    for i in allTopics:
        if i in t:
            tmpList.append(1)
        else:
            tmpList.append(0)
    numList.append(tmpList)
'''
for l in numList:
    print l
'''
numOfVisits = numList.__len__()

X = np.array(numList)
kmeans = KMeans(n_clusters=NUM_CLUSTERS, random_state=0).fit(X)
#print kmeans
labels = kmeans.labels_
centers = kmeans.cluster_centers_


clusters = []
for i in range(0, NUM_CLUSTERS):
    tmp = []
    clusters.append(tmp)

for i in range(0,numOfVisits):
    clusters[labels[i]].append(numList[i])

arrs = []
for i in range (0, NUM_CLUSTERS):
    arr = np.array(clusters[i])
    arrs.append(arr)

sums = []
for i in range (0, NUM_CLUSTERS):
    sums.append(arrs[i].sum(axis=0))

ratios = []
for sum in sums:
    tmpSum = sum.sum(axis=0)
    tmpRatios = []
    for s in sum:
        tmpRatios.append(round((float)(s)/tmpSum, 3))
    ratios.append(tmpRatios)


for i in range(0, NUM_CLUSTERS):
    print "Cluster " + str(i)
    for j in range (0, numOfTopics):
        if ratios[i][j] >= MIN_RATIO:
            print allTopics[j] + " - " + str(ratios[i][j]*100) + " %    "
    print "\n"

f = open("clusters.csv", 'w')
f.write("topic_name,cluster_0_abs,cluster_0_rel,cluster_1_abs,cluster_1_rel,cluster_2_abs,cluster_2_rel,cluster_3_abs,cluster_3_rel,cluster_4_abs,cluster_4_rel,cluster_5_abs,cluster_5_rel,cluster_6_abs,cluster_6_rel,cluster_7_abs,cluster_7_rel\n")
for j in range(0, numOfTopics):
    f.write(allTopics[j])
    for i in range(0, NUM_CLUSTERS):
        f.write("," + str(sums[i][j]))
        f.write("," + str(ratios[i][j]))
    f.write("\n")
f.close()

